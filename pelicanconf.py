#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

THEME = 'themes/entanglement'
PLUGIN_PATHS = ['plugins']
#PLUGINS = ['pelican-toc']


SITENAME = 'Entanglement·Garden'
SITESUBTITLE = ''
SITEURL = 'https://entanglement.garden'
#SITEURL = 'http://127.0.0.1:8000'
#SITEURL = '/'
SITE_SOURCE_URL = 'https://codeberg.org/EntanglementGarden/www.git'

AUTHOR = 'Entanglement Gardeners'
AUTHOR_URL = False
AUTHOR_SAVE_AS = False
AUTHORS_URL = False
AUTHORS_SAVE_AS = False

ARTICLE_PATHS = ['blog']
ARTICLE_URL = 'blog/{category}/{slug}/'
ARTICLE_SAVE_AS = 'blog/{category}/{slug}/index.html'

CATEGORY_SAVE_AS = 'blog/{slug}/index.html'
CATEGORY_URL = 'blog/{slug}/'

PAGE_PATHS = ['pages']
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
#DISPLAY_PAGES_ON_MENU = False
#DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = [
        ('Components', '/components.html'),
        ('Git', 'https://codeberg.org/EntanglementGarden/'),
        ('IRC', 'ircs://oftc.org/#entanglement')
        ]

TAG_SAVE_AS = False
TAG_URL = False

INDEX_SAVE_AS = 'blog/index.html'

#PATH = 'content'
STATIC_PATHS = [
        'img',
        ]


TIMEZONE = 'UTC'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = (('github', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
