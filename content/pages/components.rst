
:url:
:title: Components
:save_as: components.html
:slug: components.html
:date: 2023-03-30

Rhyzome
=======

Git: https://codeberg.org/EntanglementGarden/rhyzome.git


Provisions and controls instance (VM) lifecycles using `libvirt <https://libvirt.org>`_

It exposes a RESTful API.


WebUI
=====

Git: https://codeberg.org/EntanglementGarden/webui.git

Web-based control panel for managing VMs, networks, and various other components.

.. image:: /img/webui_demo.png


Network
=======

Git: https://codeberg.org/EntanglementGarden/networking.git

Creates private networks and `Wireguard <https://www.wireguard.com/>`_ tunnels.

IAM
===

Git: https://codeberg.org/EntanglementGarden/iam.git

IAM = "Identity and Access Management"

Built on `Ory Kratos <https://github.com/ory/kratos>`_
