
:url:
:save_as: rhyzome/index.html
:slug: rhyzome
:goimport: entanglement.garden/rhyzome git https://codeberg.org/EntanglementGarden/rhyzome.git


***********
Hypervisors
***********

- `source <https://codeberg.org/EntanglementGarden/rhyzome>`__


The Rhyzome agent runs on hypervisors performing CRUD operations on instances via `libvirt`.
It connects to the central Rhyzome server, exposing a RESTful API.
